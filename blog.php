<?php
	include('header.php');
?>

<section id="mainTop" class="clearfix">
<div>
	<div>
		<h1 class="art">Read Our Latest Posts. Learn Something New Maybe!</h1>
	</div>
	<div class="sub-menu">
		<a href="#" class="menu-color selected" style="text-decoration:none;" onMouseover="style.color='red';" onMouseout="style.color='black';">ALL</a>
		<a href="#" class="menu-color" style="text-decoration:none;" onMouseover="style.color='red';" onMouseout="style.color='black';"> / NEWS</a>
		<a href="blog.php" class="menu-color" style="text-decoration:none;" onMouseover="style.color='red';" onMouseout="style.color='black';"> / DESIGN</a>
		<a href="#" class="menu-color" style="text-decoration:none;" onMouseover="style.color='red';" onMouseout="style.color='black';"> / PRINT</a>
		<a href="#" class="menu-color" style="text-decoration:none;" onMouseover="style.color='red';" onMouseout="style.color='black';"> / ART</a>
		<a href="#" class="menu-color" style="text-decoration:none;" onMouseover="style.color='red';" onMouseout="style.color='black';"> / DEVELOPMENT</a>
	</div>
</div>
	<div class="sec-one">
		<article><a href="blog_item.php"><img src="img/blog-1.png" onmouseover="this.src=this.src.replace('blog-1','blog-1-trans')" 
			onmouseout="this.src=this.src.replace('blog-1-trans','blog-1')"  alt="img"/></a></article>
		<div>
			<p class="article-art">Sticker Mule</p>
			<p class="small-date"><small>June 15, 2012 / news, contests</small></p>
			<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
			<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
		</div>
	</div>		
	<div class="sec-two">
		<article><img src="img/blog-2.png"/></article>
		<div>
			<p class="article-art">10 Amazing Websites</p>
			<p class="small-date"><small>June 15, 2012</small></p>
			<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
			<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
		</div>
	</div>	
	<div class="sec-three">
		<article><img src="img/blog-3.png"/></article>
		<div>
			<p class="article-art">Top iPhone Apps</p>
			<p class="small-date"><small>June 15, 2012</small></p>
			<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
			<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
		</div>
	</div>	
	<div class="sec-four">
		<article><img src="img/blog-4.png"/></article>
			<div>
				<p class="article-art">Photo Shots</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>	
	<div class="sec-five">
		<article><img src="img/blog-5.png"/></article>
			<div>
				<p class="article-art">Big Buck Bunny</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>
	<div class="sec-six">
		<article><img src="img/blog-6.png"/></article>
			<div>
				<p class="article-art">Charakter Design</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>
	<div class="sec-seven">
		<article><img src="img/blog-7.png"/></article>
			<div>
				<p class="article-art">Service Icons</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>
	<div class="sec-eight">
		<article><img src="img/blog-8.png"/></article>
			<div>
				<p class="article-art">Wedding Card</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>
	<div class="sec-nine">
		<article><img src="img/blog-9.png" style="margin-top: 21px;"/></article>
			<div>
				<p class="article-art">Pinterest Icons</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>
	<div class="sec-ten">
		<article><img src="img/blog-10.png"/></article>
			<div>
				<p class="article-art">iPad 3 Review</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
		</div>
	</div>
	<div class="sec-eleven">
		<article><img src="img/blog-11.png"/></article>
			<div>
				<p class="article-art">Social Media Buttons</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" /> More</a>				
			</div>
	</div>
	<div class="sec-twelve">
		<article><img src="img/blog-12.png"/></article>
			<div>
				<p class="article-art">Silver UI Kit</p>
				<p class="small-date"><small>June 15, 2012</small></p>
				<p class="txt-pos">Lorem ipsum dolor sit amet, te
				possim inimicus ius. Alii ullam at
				corper pri ad, per nulla luptatum 
				te, in qui delenit nostrum. Nam ad
				labores.</p>
				<a href="" style="text-decoration:none; color: black;"><img src="img/more2.png" alt="html" id="blog-1-pos" />More</a>				
			</div>
	</div>
</section>
</section>
	<section class="sec-button">
		<button type="Submit" class="btn-footer">1</button>
		<button type="Submit" class="btn-footer2">2</button>
		<button type="Submit" class="btn-footer">3</button>
		<button type="Submit" class="btn-footer">4</button>
	</section>
	<div class="mar-foot"></div>
<?php
include('footer.php');
?>










