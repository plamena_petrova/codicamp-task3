<?php //include 'env.php'; 

	// filename
	$filename = basename($_SERVER['PHP_SELF'],".php");
	$directory = '/' . basename(dirname($_SERVER['PHP_SELF']));

	// domain
	//$domain_name = 'http://www.design2html.org/';

	// Preparing the base href string as it needs the hostname for clarification
	$basehref =
		'http' . (!isset($_SERVER['HTTPS']) ? null : 's' ) . '://'
		. $_SERVER['HTTP_HOST']
		. strtr(dirname($_SERVER['SCRIPT_FILENAME']), array($_SERVER['DOCUMENT_ROOT'] => ''))
		. '/';
	//var_dump($_SERVER); die();
	/**#@-*/

	// project name
	$project_name = 'Boilerplate Prototype Template';
	
	if(!isset($page_title) || !$page_title){
		$page_title = 'Home';
	}
?>
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js eq-ie9 ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<title>Gridzilla</title>
    <meta charset="utf-8">
	<script src="js/application.js"></script>
	<meta charset="utf-8">	
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="<?php echo $basehref; ?>">
    <title><?php echo $page_title; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/custom.css">
	 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <?php if($page_title == 'Contacts') { ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXSvNhrGGECmJMZKs3Ewbz32jVRvLTTq4&language=en"></script>
    <?php } ?>	
	<script src="js/libs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript">
        var currentPage = '<?php echo $page_title; ?>';
    </script>
	    <script>
        if (typeof jQuery == 'undefined') {
            var e = document.createElement('script');
            e.src = "js/libs/jquery-1.8.3.min.js";
            e.type='text/javascript';
            document.getElementsByTagName("head")[0].appendChild(e);
        }
		</script>
		<style>
		.carousel-inner > .item > img,
		.carousel-inner > .item > a > img {
		width: 70%;
		margin: auto;
		}
		</style>
</head>
<body class="body_bgr">
<div class="main">
<header>
    <div class="main">
			<div class="logo">
				<a href="index.php" class="gridzilla" style="text-decoration:none;">Gridzilla<span class="gridzilla-point">.</span></a>
			</div>
			<div>
				<input class="search" type="text" placeholder="  Enter your search..." /><a href=""><img id="iconSearch" src="img/icon_search.png"/><a>
				</br></br>
			</div>		
		 <div>
			<nav class="nav-menu">
				<a class="menu-color selected" href="#"><span>HOME</span></a>
				<a class="menu-color" href="about.php" onMouseover="style.color='red';" onMouseout="style.color='black';"><span>ABOUT</span></a>
				<a class="menu-color" href="blog.php" onMouseover="style.color='red';" onMouseout="style.color='black';"><span>BLOG</span></a>
				<a class="menu-color" href="contacts.php" onMouseover="style.color='red';" onMouseout="style.color='black';"><span>CONTACT</span></a>
			</nav>
		</div>
	</div>
</header>