<?php
	$page_title = 'Home';
	include('header.php');
?>
<div class="pos-blog-item" class="main">
	<div>
		<h1 class="art">Sticker Mule. Best Place For You Sticker Needs!</h1>
	</div>
	<div class="pos-blog-item-mar"><img src="img/blog-item-1.png"></div>
	<div>
		<div class="blog-item-right">
		<table>
			<tr>
				<th class="left">Date</th>
				<th class="left2">Tags</th>
				<th style="padding-left: 45px;">Author</th>
			</tr>
			<tr>
				<td class="red">April 15, 2012</td>
				<td class="red left">Website | Design</td>
				<td class="red left">Michael Reimer</td>
			</tr>
		</table>
		<div>
				<p class="pos-top">Lorem ipsum dolor sit amet, mollis epicuri pri ei, perpetua
				honestatis ad vix. Ne duo ludus putent, cu causae tamquam 
				voluptua duo. Agam officiis no duo, ut reque decore  sea. Cu
				eripuit accusam vix. Facete blandit detraxit pri cu, sea soluta 
				doming civibus ea.</p>
	
				<p>Eum eu tale clita iuvaret, cu est saperet forensibus interesset, cum
				ne case iusto oportere. Id idque idoctum eum, menandri mediocrem has ei. At usu modo auaerendum. Sit ei dicunt 
				tacimates, mea ea enim airmod suscipiantur, amet dicit ancille vel 
				in. Ex mea augue eloquentium, his postea dolorem suavitate ea. 
				Mel hendrerit accommodare concludaturque ex.
				</p>
		</div>
			<div>
				<a href="" class="more-btn more" id="right"><img src="img/more2.png"/><strong class="web-site">Visit Website</strong></a>
			</div>
		</div>
	</div>

	<div class="top">
		<img src="img/blog-item-2.png"/>
	</div>
	<div class="top">
		<h1 class="art">Similar Posts. Check them Out!</h1>
	</div>
	<div class="clearfix">
		<div class="sec-blog-itm1">
			<article><img src="img/blog-item-3.png"/></article>
				<div>
					<p class="sec-blog-itm-p1">Charakter Design</p>
					<p class="sec-blog-itm-p2"><small>June 15, 2012</small></p>				
				</div>
		</div>
		<div class="sec-blog-itm2">
			<article><img src="img/blog-item-4.png"/></article>
				<div>
					<p class="sec-blog-itm-p1">Top iPhone Apps</p>
					<p class="sec-blog-itm-p2"><small>June 15, 2012</small></p>			
				</div>
		</div>
		<div class="sec-blog-itm3">
			<article><img src="img/blog-item-5.png"/></article>
				<div>
					<p class="sec-blog-itm-p1">Social Media Buttons</p>
					<p class="sec-blog-itm-p2"><small>June 15, 2012</small></p>				
				</div>
		</div>
		<div class="clearfix sec-blog-itm4">
			<article><img src="img/blog-item-6.png"/></article>
				<div>
					<p class="sec-blog-itm-p1">10 Amaizing Websites</p>
					<p class="sec-blog-itm-p2"><small>June 15, 2012</small></p>	
				</div>
		</div>
	</div>
</div>
<?php
include('footer.php');
?>
