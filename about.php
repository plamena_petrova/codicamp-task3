<?php
	$page_title = 'About';
	include('header.php');
?>
<!DOCTYPE html>
	<div class="main-home" id="mar-left"> 
		<div id="aboutTop">
			<h1 class="art">About Us. Our Mission. Shortcodes. Tones More!</h1>
		</div>
			<div>
				<div class="font-blog">
					<p >
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante diapibus diam. Sed nisi. Nulla quis sem at nibh elementum 
					imperdiet. Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis. 
					</p>
				</div>
				<div>
						<img src="img/about.png" />
				</div>
				<div>
					<p id="about-p1">Maecenas ipsum metus, semper hendrerit varius mattis, hasr cingue sit amet tellus. Aliquam ullamcorper dui sed magna 
					posuere ut elem entum rutrum. Nam mi erat, porta idso ultrices nec, pellentesq ue vel nunc. Cras varius fermentum 
					iaculis. Aenean sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet
					et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae.</p>
					<p id="about-p2">Pallentesque ultrices nisl quis odio posuere facsilis. In ut felis erat, ac laoreet otci. 
					Aenean sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae. Pallentesque ultrices nisl quis odio posuere facsilis. In ut felis erat, ac laoreet otci. 
					Aenean sodales nibh non lectus tempor a interdumni justo ultrices.</p>
					<p>Pallentesque ultrices nisl quis odio posuere facsilis. In ut felis erat, ac laoreet otci. 
					Aenean sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae. Pallentesque ultrices nisl quis odio posuere facsilis. In ut felis erat, ac laoreet otci. 
					Aenean sodales nibh non lectus tempor a interdumni justo ultrices.</p>
					<p>Pallentesque ultrices nisl quis odio posuere facsilis. In ut felis erat, ac laoreet otci. 
					Aenean sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae.</p>
				</div>
			</div>
			<div>
			<div>
				<span id="label">D</span>
					<p id="label-p1">Quisque sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis
					tincidunt erat, hendrerit nibh iaculis vitae.</p>
				<label id="label2">&#44;&#44;</label>
					<p style=""><i>Quisque sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae.Quisque sodales nibh non lectus tempor a interdumni justo ultrices. Sed luctus dui nec ni si item pus fausibus sit amet et sem.
					Anean augue sapien, sodales ac bibendum ut, pellentesq id eros. Duis tristaque porta aliquam. Curabitur sagittis 
					tincidunt erat, hendrerit nibh iaculis vitae.</i></p>
			</div>			
			</div>
			<div>
				<div><p class="about-div-p1">Quisque mollis, sem id laoreet pretium. lectus elit molestie urna, id tristaque risus ante at est. Sed pretium 
				metus sit amet erat blandit vitae pulvinar lorem
				egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem.</p>
				</div>
				<div><p class="about-div-p2">Quisque mollis, sem id laoreet pretium. lectus elit molestie urna, id tristaque risus ante at est. Sed pretium 
				metus sit amet erat blandit vitae pulvinar lorem
				egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem.</p>
				</div>
			</div>
			<div class="about-art">
			<div>
				<div><p class="about2-div-p1">Quisque mollis, sem id laoreet pretium. lectus elit molestie urna, id tristaque risus ante at est. Sed pretium 
				metus sit amet erat blandit vitae pulvinar lorem
				egestas.</p>
				</div>
				<div><p class="about2-div-p2">Quisque mollis, sem id laoreet pretium. lectus elit molestie urna, id tristaque risus ante at est. Sed pretium 
				metus sit amet erat blandit vitae pulvinar lorem
				egestas.</p>
				</div>
				<div>
				<p class="about2-div-p3">Quisque mollis, sem id laoreet pretium. lectus elit molestie urna, id tristaque risus ante at est. Sed pretium 
				metus sit amet erat blandit vitae pulvinar lorem
				egestas.</p>
				</div>
			</div>
			</div>
			<div class="art-about">
			<div>
				<div class="float-left">
					<button type="button" class="about-btn1">Button Text</button>
				</div>
				<div class="about-div">
					<button type="button" class="about-btn2">Button Text</button>
				</div>
				<div class="about-div">
					<button type="button" class="about-btn3">Button Text</button>
				</div>
				<div class="about-div"">
					<button type="button" class="about-btn4">Button Text</button>
				</div>
				<div class="about-div">
					<button type="button" class="about-btn5">Button Text</button>
				</div>
				<div class="about-div">
					<button type="button" class="about-btn6">Button Text</button>
				</div>
				<div class="about-div">
					<button type="button" class="about-btn7">Button Text</button>
				</div>
			</div>
			</div>
			<div class="alert-div">
				<div class="alert-box1"><p align="center" class="alert-box1-txt"><i>Alert Text</i></p></div>
				<div class="alert-box2"><p align="center" class="alert-box2-txt"><i>Alert Text</i></p></div>
				<div class="alert-box3"><p align="center" class="alert-box3-txt"><i>Alert Text</i></p></div>
				<div class="alert-box4"><p align="center" class="alert-box4-txt"><i>Alert Text</i></p></div>
			</div>
</div>
<?php
include('footer.php');
?>





